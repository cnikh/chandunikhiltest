---
layout: post
title:  "Markdown Example"
author: john
categories: [ Jekyll, tutorial ]
image: assets/images/10.jpg
---
You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes. You can rebuild the site in many different ways, but the most common way is to run `jekyll serve`, which launches a web server and auto-regenerates your site when a file is updated. Something.

To add new posts , just add a file in the `_posts` directory that follows the convention `YYYY-MM-DD-name-of-post.ext` and includes the necessary front matter. Take a look at the source for this post to get an idea about how it works.

Jekyll also offers powerful support for code snippets:

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

<h2>Example of code</h2>

<pre>
    <div class="container">
        <div class="block two first">
            <h2>Your title</h2>
            <div class="wrap">
              
<code> 

<blockquote class="embedly-card"><h4><a href="https://stackblitz.com/edit/angular-forms-demo?ctl=1&embed=1&file=src/main.ts&view=preview">
angular-forms-demo - StackBlitz</a></h4><p>Angular Example - Forms</p></blockquote>
<script  src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
            </div>
        </div>
    </div>
</pre>

</code>


[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/