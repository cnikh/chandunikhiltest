---
layout: page
title: About Me
comments: true
---

Hello, my name is Nikhil Chandu, and I've been working as a Full stack developer for 7+ years.


I have extensive experience building and maintaining web server applications of all shapes and sizes.
The majority of my experience is in Java and Angular, but I also have experience with many other languages as well. 
I have a strong understanding of the entire stack of server-side technologies.


My skills;

- Java,Struts2,Spring Framework,Spring boot, Angular,JavaScript,jQuery, PHP, Bash, HTML, XML, YAML, CSS,SCSS,SASS,Android,Nativescript,Flutter
- GitHub, Git, Eclipse IDE, CircleCI, Visual Studio, Apache Tomcat
- Heroku, AWS EC2, WordPress, Amazon Web Services (AWS), Unix, AWS Elastic Beanstalk, Windows, JBoss
- REST APIs, JSON API, jQuery
- Web Services, AWS Route 53
